#include<Arduino.h>

#define pinD13 13
#define pinred 2
#define pingreen 3
#define buphsize 40
#define t_buphsize 10

#define t_EOL 255
#define t_ERR 254
#define t_base 128
#define t_D13 129
#define t_RED 130
#define t_GREEN 131
#define t_ON 132
#define t_OFF 133
#define t_BLINK 134
#define t_SET 135
#define t_WORD 136
#define t_STATUS 137
#define t_VERSION 138
#define t_HELP 139

char bupher[buphsize];
byte t_bupher[t_buphsize];
byte buphidx = 0;
unsigned long previous_millis = 0;
int blink_period = 500;
bool debug_uno = true;
char keyin;

byte t_table[] = {
  'D', '1', 3, t_D13,
  'R', 'E', 3, t_RED,
  'G', 'R', 5, t_GREEN,
  'O', 'N', 2, t_ON,
  'O', 'F', 3, t_OFF,
  'B', 'L', 5, t_BLINK,
  'S', 'E', 3, t_SET,
  'W', 'O', 4, t_WORD,
  'S', 'T', 6, t_STATUS,
  'V', 'E', 7, t_VERSION,
  'H', 'E', 4, t_HELP,
  0,0,0,0,0
};

byte SNX = 0; //start pointer for first non space character
byte ENX = 0; //pointer for end of 
byte idxt;  //lookup table index
byte tbidx = 0; //token buffer index
bool is_t_found = false;
word numero = 0;
bool delete_t_bupher = false;
bool is_time_to_eval_bupher = false;
/*+++++++++++++++++++++++++++++++++++++++++++++++
 *readSer function used to fill a buffer, then when cr enters the buffer 
 *sets flag to evaluate the buffer
 *++++++++++++++++++++++++++++++++++++++++++++++*/
void readSer(){
  if(Serial.available() > 0){
    keyin = Serial.read();
    if((keyin >= 32) && (keyin < 127) && (buphidx < buphsize - 1)){  //32 is ASCII Space, 127 is delete. Normal characters in this range
      bupher[buphidx++] = toupper(keyin);
      Serial.print(keyin);
      Serial.println("");     
    }
    if(keyin == 13){   //13 is ASCII cr
      bupher[buphidx] = '\0';
      if(debug_uno == true){
        for(int i = 0; i < buphidx; i++){
          Serial.print("  buffer index - ");
          Serial.print(i);
          Serial.print(" -is- ");
          Serial.println(bupher[i]);
          is_time_to_eval_bupher = true;
        }      
      }
      buphidx = 0;              
    }
    if((keyin == 127) && (buphidx > 0)){   //127 is ASCII Delete
      buphidx--;
      Serial.print("\b \b"); 
    }
  }//end if ser avail
}//end function
/*++++++++++++++++++++++++++++++++++++++++++++++++
 * eval read buffer, evaluates the read buffer * 
 * 
 */

void eval_read_buffer(){
  if(true == is_time_to_eval_bupher){
    SNX = 0;
    ENX = 0;
    tbidx = 0;

    while(bupher[SNX] != '\0'){
      while(bupher[SNX] != '\0' && bupher[SNX] == ' '){
        SNX++;
      }
      ENX = SNX;
      while((bupher[ENX] != ' ') && (bupher[ENX] != '\0')){
        ENX++;  //Counts until next space or EOL is found
        is_t_found = false;
        idxt = 0;
        numero = 0;
      }
      if(ENX - SNX > 1){ //if valid Check against lookup table
        while(t_table[idxt] != 0){
          if((bupher[SNX] == t_table[idxt]) && (bupher[SNX + 1] == t_table[idxt + 1]) && (ENX - SNX == t_table[idxt + 2])){
            is_t_found = true; //flag set when valid token found from table
            t_bupher[tbidx++] = t_table[idxt + 3];
            break; //break while loop if found 
          }
          else {
            idxt += 4;
          }
        }
        if(is_t_found == false){//ifnot in lookup table, is Number? 
          idxt = SNX;
          while(idxt < ENX){
            if(bupher[idxt] < '0' || bupher[idxt] > '9'){ //if out of range
              if(debug_uno == true){
                Serial.println(" Weird Input there fella, Try Again ");                               
              }
              numero = 0xFFFF;
              break;
            }
            else {
              numero = numero * 10;
              numero += (bupher[idxt] - '0');
              idxt++;                
            }
          }
          if(numero <= 0xE000){
            if(numero >= 127){
              t_bupher[tbidx++] = t_WORD;
              t_bupher[tbidx++] = numero & 0x007F;
              t_bupher[tbidx++] = (numero >> 7) & 0x007F;
            }
            else {
              t_bupher[tbidx++] = numero & 0x007F;
            }
          }
          else {
            t_bupher[tbidx++] = t_ERR; //Not valid input
          }
        }
      }
      SNX = ENX + 1;
    }
    if(bupher[SNX] == '\0'){
      t_bupher[tbidx] = t_EOL;
    }
    is_time_to_eval_bupher = false;
    if(debug_uno == true){
      for(int i = 0; i < t_buphsize; i++){
        Serial.print("  t_buffer index - ");
        Serial.print(i);
        Serial.print(" -is- ");
        Serial.println(t_bupher[i]);
      }
    }
   for(int i = 0; i < buphsize; i++){
    bupher[i] = 0;    
   }
  }
}

void setup() {
  Serial.begin(9600);
}

void loop() {
  readSer();
  eval_read_buffer(); 
}
