#include<Arduino.h>

#define pinD13 13
#define pinred 2
#define pingreen 3
#define buphsize 40
#define t_buphsize 10

#define t_EOL 255
#define t_ERR 254
#define t_base 128
#define t_D13 129
#define t_RED 130
#define t_GREEN 131
#define t_ON 132
#define t_OFF 133
#define t_BLINK 134
#define t_SET 135
#define t_WORD 136
#define t_STATUS 137
#define t_VERSION 138
#define t_HELP 139

char bupher[buphsize];
byte t_bupher[t_buphsize];
byte buphidx = 0;
unsigned long previous_millis = 0;
int blink_period = 500;
bool debug_uno = true;




byte t_table[] = {
  'D', '1', 3, t_D13,
  'R', 'E', 3, t_RED,
  'G', 'R', 5, t_GREEN,
  'O', 'N', 2, t_ON,
  'O', 'F', 3, t_OFF,
  'B', 'L', 5, t_BLINK,
  'S', 'E', 3, t_SET,
  'W', 'O', 4, t_WORD,
  'S', 'T', 6, t_STATUS,
  'V', 'E', 7, t_VERSION,
  'H', 'E', 4, t_HELP,
};

byte SNX = 0; //start pointer for first non space character
byte ENX = 0; //pointer for end of 
byte idxt;  //lookup table index
byte tbidx = 0; //token buffer index
bool is_t_found = false;
word numero = 0;
bool delete_t_bupher = false;

void readSer(){
  char keyin;

  keyin = Serial.read();
  if((keyin >= 32) && (keyin < 127) && (buphidx < buphsize - 1)){  //32 is ASCII Space, 127 is delete. Normal characters in this range
    bupher[buphidx++] = toupper(keyin);
    Serial.print(keyin);
  }
  else if(keyin == 13){   //13 is ASCII cr
    bupher[buphidx] = '\0';
    Serial.println("");
    if(debug_uno == true){
      for(int i = 0; i < buphidx; i++){
        Serial.print("LN65-");
        Serial.print(bupher[i]);
      }      
    }

    toke_time();
//    print_token();
    buphidx = 0;

  }
  else if((keyin == 127) && (buphidx > 0)){   //127 is ASCII Delete
    buphidx--;
    Serial.print("\b \b");    
  }
}

void toke_time(){
    if(delete_t_bupher == true){
    for(int i = 0; i < t_buphsize; i++){
      t_bupher[i] = 0;
    }
    delete_t_bupher = false;
  } 
  
  SNX = 0;
  ENX = 0;
  tbidx = 0;
  
  
  while(bupher[SNX] != '\0'){
    while(bupher[SNX] != '\0' && bupher[SNX] == ' '){
      SNX++;
    }
    ENX = SNX;
    while((bupher[ENX] != ' ') && (bupher[ENX] != '\0')){
      ENX++;  //Counts until next space or EOL is found 
      is_t_found = false;
      idxt = 0;
      numero = 0;     
    }

    if(ENX - SNX >= 1){ //Check against lookup table
      while(t_table[idxt] != 0){
//        if((bupher[SNX] != t_table[idxt]) && (bupher[SNX + 1] != t_table[idxt + 1]) && ((ENX + 1) - SNX != t_table[idxt + 2])){
//          idxt += 4;
//          is_t_found = false;
//        }

        if((bupher[SNX] == t_table[idxt]) && (bupher[SNX + 1] == t_table[idxt + 1]) && (ENX - SNX == t_table[idxt + 2])){
          is_t_found = true; 
          t_bupher[tbidx] = t_table[idxt + 3]; 
          tbidx++; 
          
          break;
        }else{idxt += 4;}
        
      }
    }
      if(is_t_found == false){
        idxt = SNX;
        while(idxt <= ENX){  
          if(bupher[idxt] < '0' || bupher[idxt] > '9'){ //if out of range
            if(debug_uno == true){
              Serial.println("not number ");      
            }
            numero = 0xFFFF;
            break;
          }else{numero = numero * 10; numero = (bupher[idxt] - '0');} //covnert to dec
          idxt++;          
        }
        if(numero <= 0xE000){
          if(numero >= 256){
            t_bupher[tbidx] = t_WORD;
            tbidx++;
            t_bupher[tbidx] = numero & 0x00FF;
            tbidx++;
            t_bupher[tbidx] = (numero >> 8) & 0x00FF;
            tbidx++;
          }else{
          t_bupher[tbidx] = numero & 0x00FF;
          tbidx++;
          }
        }else{
          t_bupher[tbidx] = t_ERR;
          tbidx++;
        }
        
      }//end- if(is_t_found == false)
    
    SNX = ENX+1;    
  
  if(bupher[SNX] == '\0'){
    t_bupher[tbidx] = t_EOL;
  }
  toke_to_command();  
}
}


void toke_to_command(){
  if(debug_uno == true){
      for(int i = 0; i < t_buphsize; i++){
        Serial.println("");
        Serial.println("Toke-to-command");
        Serial.print(t_bupher[i]);
      }
  }
 
}


void setup() {
  Serial.begin(9600);
  

}

void loop() {
 
  if(Serial.available() > 0){
    readSer();
  }
  

  if(delete_t_bupher == true){
    for(int i = 0; i <= t_buphsize; i++){
      t_bupher[i] = 0;
    }
    delete_t_bupher = false;
  }  
}
