
/*
+Bennie's Fabulous DPS Project 2
+Last Edited Jul, 30, 2019
+
+code version */
#define code_version 012

/*"Sweet baby Jesus, coding hurts my brain!" - Albert Einstein
+uig
+
+
+
+
*/


#include<Arduino.h>
#include<SimpleDHT.h>
#include<DS3231_Simple.h>
#include<EEPROM.h>
#include<Wire.h>
#include<Adafruit_NeoPixel.h>

#define pinD13 13
#define pinred 2
#define pingreen 3
//#define pinDHT22 4
#define buphsize 40
#define t_buphsize 12
#define LED_COUNT 3  //RGB LED
#define LED_PIN 5 //FOR RGB Led's
Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);

#define t_EOL 255
#define t_ERR 254
#define t_DEBUG 253
#define t_base 128
#define t_D13 129
#define t_RED 130
#define t_GREEN 131
#define t_ON 132
#define t_OFF 133
#define t_BLINK 134
#define t_SET 135
#define t_WORD 136
#define t_STATUS 137
#define t_VERSION 138
#define t_HELP 139
#define t_LED 140
#define t_RTC 141
#define t_ADD 142
#define t_MULTIPLY 143
#define t_TEMP 144
#define t_RGB 145
#define t_TIME 146
#define t_RESETEE 147

int pinDHT22 = 4;

DS3231_Simple Clock;
SimpleDHT22 dht22(pinDHT22);

char bupher[buphsize];
byte t_bupher[t_buphsize];
byte buphidx = 0;
unsigned long previous_millis = 0;
unsigned long previous_millis_A = 0;
unsigned long previous_millis_B = 0;
word blink_period = 500;
                
bool debug_uno = true;  

bool is_time_to_eval_bupher = false; 
bool is_toke_parse_time = false; 
bool is_t_found = false;

///bi-color led NEW rotating pattern code
const byte LED_off_pat = 0b11111111;
const byte LED_red_pat = 0b10101010;
const byte LED_green_pat = 0b01010101;
const byte LED_bred_pat = 0b10111011;
const byte LED_bgreen_pat = 0b01110111;
const byte LED_bitmask = 0b00000001;
byte LED_state = 0b10111011;
byte D13_state = 0b00000000;
const byte D13_off = 0b00000000;
const byte D13_on = 0b11111111;
const byte D13_blink = 0b11001100;
byte prev_LED_color = 0b10111011;
byte LED_abitmask = 0b00000001;

////RGB LEDS
byte R;
byte G;
byte B;
byte prev_R;
byte prev_G;
byte prev_B;
bool blinkRGB;
//TEMP/HUMIDITY
unsigned int addr = 25;
byte storage[8];

/*
dht22.read2(&temperature, &humidity, NULL)
*/
float temperature = 0.0;
float humidity = 0.0;
byte mintempval = 255;
byte prevmintemp = 255;
byte maxtempval = 0;
byte prevmaxtemp = 0;
///RTC
bool is_setting_rtc = false;
DateTime MyTimestamp; //dataset for writing to RTC

char keyin;

byte t_table[] = {
  'D', '1', 3, t_D13,
  'L', 'E', 3, t_LED,
  'L', 'E', 4, t_LED,//LEDS
  'R', 'E', 3, t_RED,
  'G', 'R', 5, t_GREEN,
  'O', 'N', 2, t_ON,
  'O', 'F', 3, t_OFF,
  'B', 'L', 5, t_BLINK,
  'S', 'E', 3, t_SET,
  'W', 'O', 4, t_WORD,
  'S', 'T', 6, t_STATUS,
  'V', 'E', 7, t_VERSION,
  'H', 'E', 4, t_HELP,
  'D', 'E', 5, t_DEBUG,
  'R', 'T', 3, t_RTC,
  'A', 'D', 3, t_ADD,
  'M', 'U', 8, t_MULTIPLY,
  'T', 'E', 4, t_TEMP,
  'R', 'G', 3, t_RGB,
  'T', 'I', 4, t_TIME,
  'R', 'E', 7, t_RESETEE,
  0,0,0,0,0
};



byte SNX = 0; //start pointer for first non space character
byte ENX = 0; //pointer for valid chars before space
byte idxt;  //lookup table index
byte tbidx = 0; //token buffer index

word numero = 0;

/*+++++++++++++++++++++++++++++++++++++++++++++
        ++++++     HELP MENU  ++++++
*///+++++++++++++++++++++++++++++++++++++++++++
void print_help_menu(){
				Serial.println(F(""));
				Serial.println(F("        -HELP MENU-"));
				Serial.println(F("COMMANDS"));
				Serial.println(F("HELP          - Prints this help menu"));
				Serial.println(F("D13           - Controls on-board led"));
				Serial.println(F("                ex; D13 ON - turns D13 on"));
				Serial.println(F("                D13 OFF - turns D13 off"));
				Serial.println(F("                D13 BLINK - blinks D13 @ SET BLINK interval"));
				Serial.println(F("LED           - Controls ext. LED"));
				Serial.println(F("                ex; LED GREEN - turns LED green"));
				Serial.println(F("                LED RED - turns LED red"));
				Serial.println(F("                LED OFF - turns LED off"));
				Serial.println(F("                LED BLINK - blinks LED @ SET BLINK interval"));
				Serial.println(F("SET BLINK ### - sets blink interval for all led's 'in ms'"));
				Serial.println(F("                ex; SET BLINK 1000 - Sets blink interval to 1000ms"));
				Serial.println(F("STATUS LED    - Prints Status of all LED's"));
				Serial.println(F("VERSION       - Prints current Program Version"));
}

void reset_resetables()
{
	LED_abitmask = LED_bitmask;
}	
	

void clear_token_bupher()
{
	for(int i = 0; i < t_buphsize; i++)
	{
  t_bupher[i] = 0;    
  }
	for(int i = 0; i < buphsize; i++)
	{
  bupher[i] = 0;    
  }
}

void bad_input()
{
	Serial.println(F(" Bad input..type 'HELP' for a list of commands "));
}

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+                              +
+++++ BI Color LED Module ++++++
+                              +
ex: to change state, call when you want to change state
set_LED(t_BLINK);
set_LED(t_RED);
set_LED(t_GREEN);
set_LED(t_OFF);

in loop: updates LED's every loop
write_to_LEDS();
*/
void set_LED(byte l_s)//sets LED blink and color
{
	byte x;
	if(l_s == t_BLINK)//if blink command recieved
	{
		if(LED_state == LED_off_pat)//if led was off, blink last color
		{
			x = prev_LED_color;
		}
		else
		{
			x = LED_state;
		}
		switch(x)
		{
			case LED_bred_pat:
				LED_state = LED_red_pat;
				prev_LED_color = LED_state;
				break;
			case LED_bgreen_pat:
				LED_state = LED_green_pat;
				prev_LED_color = LED_state;
				break;
			case LED_green_pat:
				LED_state = LED_bgreen_pat;
				prev_LED_color = LED_state;
				break;
			case LED_red_pat:
				LED_state = LED_bred_pat;
				prev_LED_color = LED_state;
				break;
			default://what else could go wrong?
				break;				
		}
	}
	if(l_s == t_OFF)// turn off led
	{
		LED_state = LED_off_pat;
	}
	if(l_s == t_RED)
	{
		if(prev_LED_color == LED_bred_pat || prev_LED_color == LED_bgreen_pat)
		{
			LED_state = LED_bred_pat;
			prev_LED_color = LED_state;
		}
		else
		{
			LED_state = LED_red_pat;
			prev_LED_color = LED_state;
		}
	}
	if(l_s == t_GREEN)
	{
		if(prev_LED_color == LED_bred_pat || prev_LED_color == LED_bgreen_pat)
		{
			LED_state = LED_bgreen_pat;
			prev_LED_color = LED_state;		
		}
		else
		{
			LED_state = LED_green_pat;
			prev_LED_color = LED_state;
		}
	}
}

void rgb_blink()
{
	if(true == blinkRGB)
	{
		if(R == prev_R)
		{
			R = 0;
			G = 0;
			B = 0;
		}
		else
		{
			R = prev_R;
			G = prev_G;
			B = prev_B;
		}
	}
//	else
//	{
//		R = prev_R;
//		G = prev_G;
//		B = prev_B;
//	}
}


void write_to_LEDS()
{
	if(millis() - previous_millis >= blink_period)
	{
		digitalWrite(pinD13, D13_state & LED_abitmask);
		digitalWrite(pingreen, LED_state & LED_abitmask);
		LED_abitmask = LED_abitmask << 1;
		digitalWrite(pinred, LED_state & LED_abitmask);
		LED_abitmask = LED_abitmask << 1;
		if(LED_abitmask >= 0b10000000 || LED_abitmask == 0b00000000)
		{
			LED_abitmask = LED_bitmask;
		}
		previous_millis = millis();
		rgb_blink();
		strip.setPixelColor(0, R, G, B);
	}
}
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
END Bi Color LED Module
*/

void print_status(byte sts)
{
	switch(sts)
	{
		case t_LED:
			if(debug_uno == true)
			{
				Serial.println(F(" printing LED status..."));
			}	
			switch(D13_state)/////D13 Status
			{
				case D13_blink:
					Serial.print(F(" D13 - ON, Blink rate = "));
					Serial.print(blink_period);
					Serial.println(F(" milliseconds"));
					break;
				case D13_on:
					Serial.println(F(" D13 - ON"));
					break;
				case D13_off:
					Serial.println(F(" D13 - OFF"));
					break;
				default:
					bad_input();
					break;
			}
			switch(LED_state) /////LED Status
			{
				case LED_off_pat:
					Serial.println(F("LED is off"));
					break;
				case LED_red_pat:
					Serial.println(F("LED is Red"));
					break;
				case LED_green_pat:
					Serial.println(F("LED is Green"));
					break;
				case LED_bred_pat:// LED Blinking red
					Serial.print(F(" LED is Red, Blink rate = "));
					Serial.print(blink_period);
					Serial.println(F(" milliseconds"));
					break;
				case LED_bgreen_pat://led blinking green
					Serial.print(F(" LED is green, Blink rate = "));
					Serial.print(blink_period);
					Serial.println(F(" milliseconds"));
					break;
			}
			break;
		case t_TEMP:
			  Serial.print(F("Sample OK: "));
				Serial.print((byte)temperature); Serial.print(F(" *C, "));
				Serial.print((byte)humidity); Serial.println(F(" RH%"));	
			break;
		default:
			bad_input();
			break;
	}
}


/*+++++++++++++++++++++++++++++++++++++++++++++++
 *readSer function used to fill a buffer, then when cr enters the buffer 
 *sets flag to evaluate the buffer
 *++++++++++++++++++++++++++++++++++++++++++++++*/
void readSer()
{
  if(Serial.available() > 0)
  {
    keyin = Serial.read();
    if((keyin >= 32) && (keyin < 127) && (buphidx < buphsize - 1)) //32 is ASCII Space, 127 is delete. Normal chars in range
    { 
      bupher[buphidx++] = toupper(keyin);
      Serial.print(keyin);
      //Serial.println("");     
    }
    if(keyin == 13)   //13 is ASCII cr
    {
      bupher[buphidx] = '\0';
      Serial.println(F(""));
      if(debug_uno == true)
      {
        for(int i = 0; i < buphidx; i++)
        {
          Serial.print(F("  buffer index - "));
          Serial.print(i);
          Serial.print(F(" -is- "));
          Serial.println(bupher[i]);
        }      
      }
      is_time_to_eval_bupher = true;
      buphidx = 0;              
    }
    if((keyin == 127) && (buphidx > 0))   //127 is ASCII Delete
    {
      buphidx--;
      Serial.print("\b \b"); 
    }
  }//end if ser avail
}
//****************************//
//end +++readSer()+++ function
//****************************//

/*
Add Numbers Function
*/
void math_numbers(byte oper)
{
	uint16_t numberA = 0;
	uint16_t numberB = 0;
	switch(t_bupher[1])
	{
		case t_ERR:
		case t_EOL:
			bad_input();
			break;
		default:
			if(t_bupher[1] == t_WORD)
			{
				numberA = (t_bupher[3] << 7);
				numberA =  t_bupher[2] + numberA;
			}
			else
			{
				numberA =  t_bupher[1];
			}
			if((t_bupher[4] == t_WORD) && (t_bupher[7] == t_EOL))
			{
				numberB = (t_bupher[6] << 7);
				numberB =  t_bupher[5] + numberB;
			}
			if(t_bupher[5] == t_EOL)
			{
				if(t_bupher[1] == t_WORD)
				{
					numberB = t_bupher[4];
				}
				if(t_bupher[2] == t_WORD)
				{
					numberB = (t_bupher[4] << 7);
					numberB =  t_bupher[3] + numberB;
				}
		
			}
			if(t_bupher[3] == t_EOL)
			{
				numberB = t_bupher[2];
			}
			if((t_bupher[3] == t_EOL) || (t_bupher[5] == t_EOL) || (t_bupher[7] == t_EOL))
			{
				switch(oper)
				{
					case t_ADD:
						Serial.println("");
						Serial.print(numberA);
						Serial.print(" + ");
						Serial.print(numberB);
						Serial.print(" = ");
						Serial.println((numberA + numberB));
						break;
					case t_MULTIPLY:
						Serial.println("");
						Serial.print(numberA);
						Serial.print(" * ");
						Serial.print(numberB);
						Serial.print(" = ");
						Serial.println((numberA * numberB));				
				}
			}
	}
}

void read_data_stor()
{
	prevmaxtemp = 0;
	prevmintemp = 255;
	word address = 25;
	while(address < addr)
	{
		for(int i = 0; i < 8; i++)
		{
			storage[i] = EEPROM.read(address++);

		}
		maxtempval = storage[6];
		mintempval = storage[6];
		maxtempval = max(maxtempval, prevmaxtemp);
		prevmaxtemp = maxtempval;		
		mintempval = min(mintempval, prevmintemp);
		prevmintemp = mintempval;
				
		Serial.print(storage[0]);
		Serial.print("/");

		Serial.print(storage[1]);
		Serial.print("/");

		Serial.print(storage[2]);
		Serial.print(" ");

		Serial.print(storage[3]);
		Serial.print(":");

		Serial.print(storage[4]);
		Serial.print(":");

		Serial.print(storage[5]);
		Serial.print(" - ");

		Serial.print(storage[6]);
		Serial.print("C - ");

		Serial.print(storage[7]);	
		Serial.print("%RH");
		
		Serial.print(" from addr ");
		Serial.print(address);
		Serial.println("");
		

	}
	Serial.print(prevmintemp);
	Serial.println(" - Lowest Temp ");
	Serial.print(prevmaxtemp);
	Serial.println(" - Highest Temp ");
}


void t_STATUS_r()
{
	if(t_EOL == t_bupher[2])
	{
		switch(t_bupher[1])
		{
		case t_LED:
			print_status(t_LED);
			break;
		case t_TEMP:
			print_status(t_TEMP);
			break;
		default:
			bad_input();
			break;
		}
	}
	else if(t_EOL == t_bupher[1])
	{
		read_data_stor();
	}
	else
	{
		bad_input();
	}
}

void t_SET_r()
{
	if((t_EOL == t_bupher[3] || t_EOL == t_bupher[5]) && (t_BLINK == t_bupher[1]))
	{
		switch(t_bupher[2])
		{
			case t_WORD:
				{
					if(debug_uno == true)
					{
						Serial.println("set_blink_word");
					}	
					word x = t_bupher[4];
					x = (x << 7);
					blink_period = x + t_bupher[3];
					if(debug_uno == true)
					{
						Serial.println(blink_period);
					}
				break;
				}
			default:
				{
					if(debug_uno == true)
					{
						Serial.println("set_blink");
					}	
					blink_period = t_bupher[2];
					if(debug_uno == true)
					{
						Serial.println(blink_period);
					}
				}
				break;
		}
	}
	else
	{
		bad_input();
	}
}

void t_LED_r()
{
	if(t_bupher[2] == t_EOL)
	{
		switch(t_bupher[1])
		{
			case t_GREEN:
				set_LED(t_GREEN);
				break;
			case t_RED:
				set_LED(t_RED);
				break;
			case t_OFF:
				set_LED(t_OFF);	
				break;
			case t_BLINK:
				set_LED(t_BLINK);			
				break;
			default:
				bad_input();
				break;
		}
	}
	else
	{
		bad_input();
	}
		
}

void t_D13_r()
{
	if(t_bupher[2] == t_EOL)
	{
		switch(t_bupher[1])
		{
			case t_ON:
				D13_state = D13_on;
				//leds[0] = CRGB::Red;
				break;
			case t_OFF:
				D13_state = D13_off;
				//leds[0] = CRGB::Black;
				break;
			case t_BLINK:
				D13_state = D13_blink;
				break;
			default:
				bad_input();
				break;
		}
	}
	else
	{
		bad_input();
	}
}

void t_RGB_r()
{
	if((t_bupher[2] == t_EOL) && (t_BLINK == t_bupher[1]))
	{
		blinkRGB = !blinkRGB;
	}
	else if(t_bupher[4] == t_EOL)
	{
		R = t_bupher[1];
		R = map(R, 0, 100, 0, 255);
		G = t_bupher[2];
		G = map(G, 0, 100, 0, 255);
		B = t_bupher[3];
		B = map(B, 0, 100, 0, 255);
		
		prev_R = R;
		prev_G = G;
		prev_B = B;
		
		
	}
	else
	{
		bad_input();
	}
}

void t_TIME_r()
{
	switch(t_bupher[1])
	{
		case t_SET:
			if(t_EOL == t_bupher[8])
			{
				MyTimestamp.Day    = t_bupher[2];
				MyTimestamp.Month  = t_bupher[3];
				MyTimestamp.Year   = t_bupher[4]; 
				MyTimestamp.Hour   = t_bupher[5];
				MyTimestamp.Minute = t_bupher[6];
				MyTimestamp.Second = t_bupher[7];
				Clock.write(MyTimestamp);
			}
			else
			{
				Serial.println(F("Enter time 'time set day month year hour minute second'"));
				Serial.println(F("ex: time set 7 22 19 16 20 33"));
				bad_input();
			}
			break;
		case t_EOL: 
			MyTimestamp = Clock.read();
			Clock.printTo(Serial, MyTimestamp);
			Serial.println();
		//	if(true = debug)
		//	{
				Serial.println(MyTimestamp.Day);
				Serial.println(MyTimestamp.Month);
				Serial.println(MyTimestamp.Year);
				Serial.println(MyTimestamp.Hour);
				Serial.println(MyTimestamp.Minute);
				Serial.println(MyTimestamp.Second);
				break;
		//	}
		}
}


/*+++++++++++++++++++++++++++++++++++++++++++++++
 *toke_parse_execute() function 
 *is_toke_parse_time is true, evaluates token buffer - t_bupher
 *executes a task based on what is in t_bupher
 *++++++++++++++++++++++++++++++++++++++++++++++*/
void toke_parse_execute()
{
	if(true == is_toke_parse_time)
	{
		is_toke_parse_time = false;
		int x = t_buphsize;
		for(int i = 0; i < x; i++)
		{
			if(t_ERR == t_bupher[i])
			{
				x = 0;
			}
		}
		if(x > 0)
		{
			switch(t_bupher[0])
			{
				//case t_ERR:
				case t_EOL:
				case t_RED:
				case t_GREEN:
				case t_ON:
				case t_OFF:
				case t_BLINK:
				case t_WORD:
				case t_TEMP:trfttttttt;/opi hgkldi9ik[iiy
				][\
				hf
	
	
	
	
	bxa99999999999999999999999999999uopjp[\\'
	
	
	
			
				default:
					bad_input();
					break;
				case t_VERSION:
					Serial.print(F("Bennie's Dope DPS Project! Version: v."));
					Serial.println(code_version);				
					break;
				case t_HELP:
					print_help_menu();
					break;
				case t_RESETEE:
					EEPROM.write(1, 25);
					addr = 25;
					break;
				case t_TIME:
					t_TIME_r();
					break;
				case t_RGB:
					t_RGB_r();
					break;
				case t_ADD:
					math_numbers(t_ADD);
					break;
				case t_MULTIPLY:
					math_numbers(t_MULTIPLY);
					break;
				case t_STATUS:
					t_STATUS_r();
					break;
				case t_SET:
					t_SET_r();
					break;
				case t_LED:
					t_LED_r();
					break;
				case t_D13:
					t_D13_r();
					break;
				case t_DEBUG:
					if(t_bupher[1] == t_EOL)
					{
						debug_uno = !debug_uno;
					}
					break;											
			}
				clear_token_bupher();
				//reset_resetables();
		}
		else
		{
			bad_input();
		}
	}	
}


/*+++++++++++++++++++++++++++++++++++++++++++++++
 *eval_read_buffer() function 
 *evaluates input character buffer
 *compares it to lookup table
 *then fills the token buffer - t_bupher
 *sets flag is_toke_parse_time to true when complete
 *++++++++++++++++++++++++++++++++++++++++++++++*/

void eval_read_buffer(){
  if(true == is_time_to_eval_bupher){
    SNX = 0;
    ENX = 0;
    tbidx = 0;

    while(bupher[SNX] != '\0')
    {
      while(bupher[SNX] != '\0' && bupher[SNX] == ' ')
      {
        SNX++;
      }
      ENX = SNX;
      while((bupher[ENX] != ' ') && (bupher[ENX] != '\0'))
      {
        ENX++;  //Counts until next space or EOL is found
        is_t_found = false;
        idxt = 0;
        numero = 0;
      }
      if(ENX - SNX >= 1) //if valid Check against lookup table
      {
        while(t_table[idxt] != 0){
          if((bupher[SNX] == t_table[idxt]) && (bupher[SNX + 1] == t_table[idxt + 1]) && (ENX - SNX == t_table[idxt + 2])){
            is_t_found = true; //flag set when valid token found from table
            t_bupher[tbidx++] = t_table[idxt + 3];
            break; //break while loop if found 
          }
          else {
            idxt += 4;
          }
        }
        if(is_t_found == false)//ifnot in lookup table, is Number? 
        {
          idxt = SNX;
          while(idxt < ENX)
          {
            if(bupher[idxt] < '0' || bupher[idxt] > '9')//if out of range
            { 
              if(debug_uno == true)
              {
                Serial.println(F(" Weird Input there fella, Try Again "));                               
              }
              numero = 0xFFFF;//16384
              break;
            }
            else
            {
              numero = numero * 10;
              numero += (bupher[idxt] - '0');
              idxt++;                
            }
          }
          if(numero <= 0x3FFF)///less than 16384
          {
            if(numero >= 127)
            {
              t_bupher[tbidx++] = t_WORD;
              t_bupher[tbidx++] = numero & 0x007F;
              t_bupher[tbidx++] = (numero >> 7) & 0x007F;
            }
            else
            {
              t_bupher[tbidx++] = numero & 0x007F; //truncate so errors don't overflow to opcode
            }
          }
          else
          {
            t_bupher[tbidx++] = t_ERR; //Not valid input
          }
        }
      }
      SNX = ENX + 1;
    }
    if(bupher[SNX] == '\0')
    {
      t_bupher[tbidx] = t_EOL;
    }
    is_time_to_eval_bupher = false;
    if(debug_uno == true)
    {
      for(int i = 0; i < t_buphsize; i++)
      {
        Serial.print(F("  t_buffer index - "));
        Serial.print(i);
        Serial.print(F(" -is- "));
        Serial.println(t_bupher[i]);
      }
    }
    is_toke_parse_time = true;
  }
}





void update_DHT()
{
  int err = SimpleDHTErrSuccess;
  if ((err = dht22.read2(&temperature, &humidity, NULL)) != SimpleDHTErrSuccess) 
  {
    Serial.print(("Read DHT22 failed, err=")); Serial.println(err);//delay(2000);
    return;
  }
}

void update_DHT_EEPROM()
{
	MyTimestamp = Clock.read();


	storage[0] = MyTimestamp.Day;
	storage[1] = MyTimestamp.Month;
	storage[2] = MyTimestamp.Year;
	storage[3] = MyTimestamp.Hour;
	storage[4] = MyTimestamp.Minute;
	storage[5] = MyTimestamp.Second;
	storage[6] = (byte)temperature;
	storage[7] = (byte)humidity;
	
	for(int i = 0; i < 8; i++)
	{
		EEPROM.write(addr++, storage[i]);
		EEPROM.write(1, addr);
		if(addr > 1017)
		{
			addr = 25;
		}
	}
	Serial.print(addr);
	Serial.println(F(" EEPROM Written to"));
}



  
  
void setup() {
  Serial.begin(9600);
  pinMode(pinD13, OUTPUT);
  pinMode(pinred, OUTPUT);
  pinMode(pingreen, OUTPUT);
  ///Blink timer
  previous_millis = millis();
  ///Loop timers
  previous_millis_A = millis();
  previous_millis_B = millis();
  ////RTC
  Clock.begin();
  strip.begin();           // INITIALIZE NeoPixel strip object (REQUIRED)
  strip.show();            // Turn OFF all pixels ASAP
  strip.setBrightness(50); // Set BRIGHTNESS to about 1/5 (max = 255)
    strip.setPixelColor(0, 255, 0, 0);  //red       //  Set pixel's color (in RAM)
    strip.setPixelColor(1, 255, 255, 0);//yellow
    strip.setPixelColor(2, 0, 255, 0);  //green
    strip.show();
    //EEPROM.write(1, 25);  //run to init eeprom log start address
    addr = EEPROM.read(1); //get last address that was written to eeprom
		prev_R = 255;
		prev_G = 0;
		prev_B = 0;
}

void loop() {
////++++++ 5 Second Loop++++++
	if(millis() - previous_millis_A >= 5000){
		update_DHT();
		previous_millis_A = millis();
	}
////++++++ 15 Minute Loop++++++
	if(millis() - previous_millis_B >= 900000){//900,000 = 15 min
		update_DHT_EEPROM();
		previous_millis_B = millis();
	}
	  
  readSer();
  eval_read_buffer();
  toke_parse_execute();
  write_to_LEDS();
  strip.show();
}
